from selenium import webdriver
from django.test import LiveServerTestCase
from selenium.webdriver.common.keys import Keys

class NewUserTest(LiveServerTestCase):

	fixtures = ['admin_user.json']

	def setUp(self):
		self.browser = webdriver.Firefox()
		self.browser.implicitly_wait(3)

	def tearDown(self):
		self.browser.quit()

	def test_can_create_a_person_using_admin_site(self):

		#Go to the admin page
		self.browser.get(self.live_server_url + '/admin/')

		#Can find the 'Django Administration' heading
		body = self.browser.find_element_by_tag_name('body')
		self.assertIn('Django administration', body.text)

		#Testing if I can login to admin
		username_field = self.browser.find_element_by_name('username')
		username_field.send_keys('root')

		password_field = self.browser.find_element_by_name('password')
		password_field.send_keys('1234')
		password_field.send_keys(Keys.RETURN)


		#Login must be successfull and taken to admin site
		body = self.browser.find_element_by_tag_name('body')
		self.assertIn('Site administration', body.text)

		#Now we see some link saying Genoapp
		genoapp_links = self.browser.find_elements_by_link_text('Genoapp')
		self.assertEquals(len(genoapp_links), 1)

		#TODO: lets create a new person using admin site
		genoapp_links[0].click()

		#Control passes to genoapp page 
		body = self.browser.find_element_by_tag_name('body')
		self.assertIn('Genoapp administration', body.text)

		#Now we must see link to Persons
		person_links = self.browser.find_elements_by_link_text('Persons')
		self.assertEquals(len(person_links), 1)

		#Going to the Persons page
		person_links[0].click()

		#Check if we are on the Persons page and insert a person using 'Add person'
		add_person = self.browser.find_elements_by_link_text('Add person')
		self.assertEquals(len(add_person), 1)

		#Lets add a person
		add_person[0].click()

		#we see some input fields for entering your information
		body = self.browser.find_element_by_tag_name('body')
		self.assertIn('First name', body.text)
		self.assertIn('LastName', body.text)
		self.assertIn('Gender', body.text)



#		self.fail('todo: finish tests')


	def testing_if_user_can_register_into_the_website(self):

		#Go to the genoapp homepage
		self.browser.get("https://intense-woodland-8718.herokuapp.com")
		#self.browser.get(self.live_server_url)


		#Can find the Homepage
		body = self.browser.find_element_by_tag_name('body')
		self.assertIn('Genograms', body.text)

		#Find the link to register a new user
		register_link = self.browser.find_elements_by_link_text('Register')
		self.assertEquals(len(register_link), 1)

		#Go to the registration page
		register_link[0].click()

		#Check for landing in the registration page
		body = self.browser.find_element_by_tag_name('body')
		self.assertIn('Please sign in', body.text)

		#Registering a random user with name test
		username_field = self.browser.find_element_by_name('username')
		username_field.send_keys('test2')

		firstname_field = self.browser.find_element_by_name('firstname')
		firstname_field.send_keys('Test')

		lastname_field = self.browser.find_element_by_name('lastname')
		lastname_field.send_keys('Subject')

		gender_field = self.browser.find_element_by_name('gender')
		gender_field.send_keys(0)

		email_field = self.browser.find_element_by_name('email')
		email_field.send_keys('test@test.com')

		birthday_field = self.browser.find_element_by_name('birthday')
		birthday_field.send_keys('12/22/1990')

		password_field = self.browser.find_element_by_name('password')
		password_field.send_keys('1234')

		password1_field = self.browser.find_element_by_name('password1')
		password1_field.send_keys('1234')

		password1_field.send_keys(Keys.RETURN)


		#Check for landing in the signin page
		body = self.browser.find_element_by_tag_name('body')
		self.assertIn('Please sign in', body.text)

		#Signing in a random user with name test
		username_field = self.browser.find_element_by_name('username')
		username_field.send_keys('test2')

		#Signing in a random user with name the password registered
		username_field = self.browser.find_element_by_name('password')
		username_field.send_keys('1234')


		#Check for landing in the user home page
		body = self.browser.find_element_by_tag_name('body')
		self.assertIn('Your Family Tree', body.text)


		


