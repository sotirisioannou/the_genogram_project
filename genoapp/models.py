from django.db import models
from django.db.models.signals import post_save
from django.contrib.auth.models import User
from django.dispatch import receiver

# Create your models here.

GENDERCHOICE = (

	('male', 'Male'),
	('female', 'Female'),
)

class Person(models.Model):
	user = models.ForeignKey(User, blank=True, null=True)
	firstName = models.CharField('first name',max_length=20)
	lastName = models.CharField(max_length=20)
	gender = models.CharField(max_length=8, choices=GENDERCHOICE)
	account = models.ForeignKey('Account')
	relationships = models.ManyToManyField('Relation' , blank = True , null = True)

	def __unicode__(self):
		return self.firstName + ' ' + self.lastName

	def full_name(self):
		user_full_name = self.firstName + ' ' + self.lastName
		return user_full_name

	def relationship_to_account_owner(self):
		account_person = Person.objects.get(user=self.account.primaryUser)
		if self.account.primaryUser == self.user:
			return "You"

		relation = self.relationships.get(to_person=account_person)
		return relation.relationship_type

	def relationship_from_account_owner(self):
		account_person = Person.objects.get(user=self.account.primaryUser)
		if self.account.primaryUser == self.user:
			return "You"

		relation = account_person.relationships.get(to_person=self)
		return relation.relationship_type



RELATIONSHIP_TYPE = (
	('father', 'Father'),
	('mother', 'Mother'),
	('son', 'Son'),
	('daughter', 'Daughter'),
	('sister', 'Sister'),
	('brother', 'Brother'),
	('wife', 'Wife'),
	('husband','Husband')
)

class Relation(models.Model):

	from_person = models.ForeignKey('Person', related_name = "from_person")
	to_person = models.ForeignKey('Person', related_name = "to_person")
	relationship_type = models.CharField('relationship_type', max_length=20, choices=RELATIONSHIP_TYPE)
	#related_to = models.ManyToManyField('Person')

	def person(self):
		return Person.objects.filter(relationship = self).values()

	def __unicode__(self):
		return self.relationship_type


class Account(models.Model):
	primaryUser = models.ForeignKey(User)
	created_on = models.DateField(auto_now_add=True)


class HealthProfile(models.Model):
	person = models.ForeignKey(Person)
	age = models.IntegerField(blank = True, null = True)
	health_issues = models.CharField(max_length=150, blank = True, null = True)
	birthday = models.DateField(null=True, blank=True)



def create_usersignin_user_callback(sender, instance, **kwargs):
	genoapp, new = UserProfile.objects.get_or_create(user=instance)
	post_save.connect(create_usersignin_user_callback, User)
