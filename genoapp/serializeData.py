from genoapp.models import Person, Relation, HealthProfile, Account, RELATIONSHIP_TYPE
from django.db.models import Q
from django.core import serializers
from django.forms.models import model_to_dict
import json

import pdb
"""Docstring for class SerializeData."""

#: This class is used for serialize the models to json format before sending them to the front end
class SerializeData(object):

    def __init__(self):
        self.json_serializer = None

#: This method accepts a model query and converts each member of the query in a format that is understandable by the
#: javascript front end.
    @staticmethod
    def serialize_graph_data(persons):

        person_list = []

#: For each person of the model's query set create an object with these fields : id , firstName , lastName,
#: full_name , gender , account id and relationships (which is an array of relationships)
        for p in persons:
            item = {
                'id' : p.id,
                'firstName' : p.firstName,
                'lastName' : p.lastName,
                'full_name': p.full_name(),
                'gender' : p.gender,
                'account_id' : p.account.id,
                'relationships' : []
            }
#: Since a person has many relationships an extra for loop is needed to loop through all the relationships of each
#: person and append them to person_list list
            for rel in p.relationships.all():
                r = {
                        'to_person' : rel.to_person.full_name(), #should print out full name
                        'to_person_id' : rel.to_person.id,
                        'relationship_type' : rel.relationship_type,
                    }

                item['relationships'].append(r)

            person_list.append(item)

        return person_list

    def getPersons(self, primary_person):

        print primary_person.lastName

#: Returns primary person ( the account holder)
    def getPrimaryPerson(self):

        current_user = request.user
        #pdb.set_trace()
        account = Account.objects.get(primaryUser = current_user)
        #pdb.set_trace()

        primary_person = Person.objects.get(primaryUser = 1)

        return primary_person

    def getParents(self, primary_person):


        primary_person_id = primary_person.id

        related_persons = Relation.objects.filter(to_person = primary_person)
        related_persons_models = []
        serializedParents = []

        for i  in range(0,len(related_persons)):
        #print related_persons[i].from_person
            related_persons_models.append(Person.objects.get(id=related_persons[i].from_person.id))
            #serializedParents.append( serializers.serialize("json", Person.objects.filter( firstName = related_persons[i].from_person)))'''

        for i in range(0,len(related_persons_models)):

            pDictionary = {}
            aPerson = related_persons_models[i]

            pDictionary = {'name' : str( aPerson.firstName + " " + aPerson.lastName) ,
                   'gender': aPerson.gender, 'id' : aPerson.id ,
                   'xPos':100 , 'yPos':1 , 'shape': None}

            if (aPerson.gender == 0):
                    pDictionary['shape'] = 'circle'
            else:
                    pDictionary['shape'] = 'rectangle'

            serializedParents.append(pDictionary)

        return serializedParents

        current_person = Person.objects.filter(user = primary_person)
        #pdb.set_trace()
        try:
            parents = current_person.filter(Q(relationships__relationship_type = 'father') | Q(relationships__relationship_type = 'mother'))
           # pdb.set_trace()
        except Person.DoesNotExist:
            parents = None

        return parents






