from django.contrib.auth.models import User
from genoapp.models import Person, Account, Relation
from rest_framework import serializers
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser


### Used for API.

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username', 'email')


class PersonSerializer(serializers.ModelSerializer):
    class Meta:
        model = Person


class AccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account


class RelationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Relation



