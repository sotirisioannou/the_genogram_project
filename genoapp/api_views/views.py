from django.contrib.auth.models import User
from genoapp.models import Person, Account, Relation
from rest_framework import viewsets
from genoapp.serializers import UserSerializer, PersonSerializer, AccountSerializer
from rest_framework import generics


class UserViewSet(viewsets.ModelViewSet):
    model = User


class PersonViewSet(viewsets.ModelViewSet):
	model = Person


class AccountViewSet(viewsets.ModelViewSet):
	model = Account
	
	
class RelationViewSet(viewsets.ModelViewSet):
	model = Relation


