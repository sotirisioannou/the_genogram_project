from django.conf.urls import patterns, url
from django.views.generic import ListView, DetailView

from genoapp import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    (r'^register/$', views.register),
)