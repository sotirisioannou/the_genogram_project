from django.conf.urls import patterns, include, url
from genoapp import views
from django.contrib import auth
from django.contrib.auth.models import User
from genoapp.models import HealthProfile, Person
from rest_framework import viewsets, routers
from rest_framework.urlpatterns import format_suffix_patterns
import genoapp.api_views.views as api_views


#from django.views.generic.simple import direct_to_template

from django.contrib import admin

admin.autodiscover()

# urlpatterns = patterns('',
#     # Examples:
#     # url(r'^$', 'genogram.views.home', name='home'),
#     # url(r'^blog/', include('blog.urls')),
#     url(r'^genoapp/', include('genoapp.urls')),
#     url(r'^admin/', include(admin.site.urls)),
# )

router = routers.DefaultRouter()
router.register(r'api/users', api_views.UserViewSet)
router.register(r'api/persons', api_views.PersonViewSet)
router.register(r'api/accounts', api_views.AccountViewSet)
router.register(r'api/relations', api_views.RelationViewSet)


urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^', include(router.urls)),
    url(r'^login/$', views.login, name='login'),
    url(r'^user/(\d)/$', views.user, name='user'),
    url(r'^registration/$', views.user_registration, name='user_registration'),
    url(r'^home/$', views.user_home, name='UserHome'),
    #url(r'^user/(\d)/$', views.user, name='user'),
    url(r'^registration/$', views.user_registration, name='user_registration'),
    url(r'^register/$', views.user_registration, name='user_registration'),
    url(r'^home/$', views.user_home, name='home'),
    #url(r'^user/(\d)/tree$', views.register, name='register'),
    url(r'^admin/', include(admin.site.urls)),
    #url(r'^home/$', views.user_home, name='UserHome'),
    url(r'^logout/$', views.logout, name='Logout'),
    url(r'^playground/$', views.playground, name='playground'),
    url(r'^add_person/$', views.add_person, name='add_person'),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
)


